#pragma once

#include <string.h>

enum number_type
{
    NUMBER_INT,
    NUMBER_UNSIGNED,
    NUMBER_DOUBLE
};

struct number
{
    number_type Type;
    union
    {
        int64_t  I;
        uint64_t U;
        double   D;
    };
};

number
Number(int64_t I)
{
    number Number;
    Number.Type = NUMBER_INT;
    Number.I = I;
    return Number;
}

number
Number(uint64_t U)
{
    number Number;
    Number.Type = NUMBER_UNSIGNED;
    Number.U = U;
    return Number;
}

number
Number(double D)
{
    number Number;
    Number.Type = NUMBER_DOUBLE;
    Number.D = D;
    return Number;
}

struct string
{
    bool EqualTo(const char *Other) const
    {
        size_t OtherLength = strlen(Other);
        if (Length != OtherLength)
        {
            return false;
        }
        else
        {
            return strncmp(Ptr, Other, Length) == 0;
        }
    }

    const char *Ptr;
    size_t Length;
};

string
String(const char *Ptr, size_t Length)
{
    string String;
    String.Ptr = Ptr;
    String.Length = Length;
    return String;
}

struct value;

struct array
{
    value *Value;
    array *Next;
};

struct object
{
    string  Name;
    value  *Value;
    object *Next;
};

enum value_type
{
    VALUE_NUMBER,
    VALUE_LITERAL,
    VALUE_ARRAY,
    VALUE_OBJECT
};

struct value
{
    value_type Type;
    union
    {
        number Number;
        string Literal;
        array Array;
        object Object;
    };
};

// Make it take 8-byte array (or something like that) and bit flags instead
typedef void (*OnNumberFunc)(void *Data, int64_t I, uint64_t U, double D, bool IsNegative, bool IsFloat);
typedef void (*OnStringFunc)(void *Data, const char *Start, size_t Length);
typedef void (*OnArrayStartFunc)(void *Data);
typedef void (*OnArrayEndFunc)(void *Data, size_t Count);
typedef void (*OnObjectStartFunc)(void *Data);
typedef void (*OnObjectEndFunc)(void *Data, size_t Count);

struct parser_client
{
    OnNumberFunc OnNumber;
    OnStringFunc OnString;
    OnArrayStartFunc OnArrayStart;
    OnArrayEndFunc OnArrayEnd;
    OnObjectStartFunc OnObjectStart;
    OnObjectEndFunc OnObjectEnd;
    void *Data;
};

struct parser
{
    const char *It;
    parser_client Client;
};