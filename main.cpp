#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <stdlib.h>
#include <assert.h>

#include "utility.h"
#include "types.h"

void
EatSpace(parser *Parser);

void
IgnoreComment(parser *Parser)
{
    ++Parser->It;
    if (*Parser->It == '/')
    {
        ++Parser->It;
        EatSpace(Parser);
        while (true)
        {
            if (*Parser->It == '\n')
            {
                ++Parser->It;
                EatSpace(Parser);
                break;
            }
            else if (*Parser->It == '\r')
            {
                ++Parser->It;
                if (*Parser->It == '\n')
                {
                    ++Parser->It;
                    EatSpace(Parser);
                    break;
                }
                else
                {
                    Error("Expected '\\n' after '\\n' but got '%c'", *Parser->It);
                }
            }
            else
            {
                ++Parser->It;
            }
        }
    }
    else if (*Parser->It == '*')
    {
        Error("Multi line comments are not implemented");
    }
    else
    {
        Error("Expected either '/' or '*' after comment start but got '%c'", *Parser->It);
    }
}

void
EatSpace(parser *Parser)
{
    while (true)
    {
        if (IsSpace(*Parser->It))
        {
            ++Parser->It;
        }
        else if (*Parser->It == '/')
        {
            IgnoreComment(Parser);
        }
        else
        {
            break;
        }
    }
}

double
GetDecimalPart(parser *Parser)
{
    double D = 0.0, DecimalPlace = 0.1;
    while (IsNumber(*Parser->It))
    {
        D += DecimalPlace * (*Parser->It++ - '0');
        DecimalPlace /= 10.0;
    }
    return D;
}

uint64_t
GetUInteger(parser *Parser)
{
    uint64_t U = 0ULL;
    while (IsNumber(*Parser->It))
    {
        U *= 10ULL;
        U += (*Parser->It++ - '0');
    }
    return U;
}

void
ParseNumber(parser *Parser)
{
    bool IsNegative = false, IsFloat = false;
    switch (*Parser->It)
    {
        case '-':
        {
            IsNegative = true;
            ++Parser->It;
            if (*Parser->It == '.')
            {
                IsFloat = true;
                ++Parser->It;
            }
            else if (!IsNumber(*Parser->It))
            {
                Error("Expected either decimal point or a number after '-' but got '%c'", *Parser->It);
            }
        } break;

        case '+':
        {
            ++Parser->It;
            if (*Parser->It == '.')
            {
                IsFloat = true;
                ++Parser->It;
            }
            else if (!IsNumber(*Parser->It))
            {
                Error("Expected either decimal point or a number after '+' but got '%c'", *Parser->It);
            }
        } break;

        case '.':
        {
            ++Parser->It;
            IsFloat = true;
            if (!IsNumber(*Parser->It))
            {
                Error("Expected a number after '.' but got '%c'", *Parser->It);
            }
        } break;
    }
    int64_t  I = 0LL;
    uint64_t U = 0ULL;
    double   D = 0.0;
    if (IsFloat)
    {
        D = GetDecimalPart(Parser);
        if (IsNegative)
        {
            D *= -1.0;
        }
    }
    else
    {
        U = GetUInteger(Parser);
        if (*Parser->It == '.')
        {
            ++Parser->It;
            IsFloat = true;
            D = GetDecimalPart(Parser);
        }
        if (IsFloat)
        {
            D += (double) U;
            if (IsNegative)
            {
                D *= -1.0;
            }
        }
        else if (IsNegative)
        {
            // Potential overflow
            I = - (int64_t) U;
        }
    }
    if (Parser->Client.OnNumber)
    {
        Parser->Client.OnNumber(Parser->Client.Data, I, U, D, IsNegative, IsFloat);
    }
}

void
ParseString(parser *Parser)
{
    const char *Start = ++Parser->It;
    while (*Parser->It != '"')
    {
        ++Parser->It;
    }
    ++Parser->It;
    if (Parser->Client.OnString)
    {
        Parser->Client.OnString(Parser->Client.Data, Start, Parser->It - Start - 1);
    }
}

void
ParseLiteral(parser *Parser)
{
    const char *Start = Parser->It;
    do
    {
       ++Parser->It; 
    }
    while (IsAlphaNumeric(*Parser->It) || *Parser->It == '_');
    if (Parser->Client.OnString)
    {
        Parser->Client.OnString(Parser->Client.Data, Start, Parser->It - Start);
    }
}

void
ParseTokenNoEatSpace(parser *Parser);

void
ParseArray(parser *Parser)
{
    if (Parser->Client.OnArrayStart)
    {
        Parser->Client.OnArrayStart(Parser->Client.Data);
    }
    size_t Count = 0;
    ++Parser->It;
    EatSpace(Parser);
    while (*Parser->It != ']')
    {
        ParseTokenNoEatSpace(Parser);
        EatSpace(Parser);
        if (*Parser->It == ',')
        {
            ++Parser->It;
            EatSpace(Parser);
            if (*Parser->It == ']')
            {
                Warning("Trailing ',' in array");
            }
        }
        else if (*Parser->It != ']')
        {
            Error("Expected either ',' or ']' after array element but got '%c'", *Parser->It);
        }
        ++Count;
    }
    ++Parser->It;
    if (Parser->Client.OnArrayEnd)
    {
        Parser->Client.OnArrayEnd(Parser->Client.Data, Count);
    }
}

void
ParseObject(parser *Parser)
{
    if (Parser->Client.OnObjectStart)
    {
        Parser->Client.OnObjectStart(Parser->Client.Data);
    }
    size_t Count = 0;
    ++Parser->It;
    EatSpace(Parser);
    while (*Parser->It != '}')
    {
        if (*Parser->It != '"')
        {
            Error("Expected '\"' at the start of object's propety name but got '%c'", *Parser->It);
        }
        ParseString(Parser);
        EatSpace(Parser);
        if (*Parser->It != ':')
        {
            Error("Expected ':' after object's property name but got '%c'", *Parser->It);
        }
        ++Parser->It;
        EatSpace(Parser);
        ParseTokenNoEatSpace(Parser);
        EatSpace(Parser);
        if (*Parser->It == ',')
        {
            ++Parser->It;
            EatSpace(Parser);
            if (*Parser->It == '}')
            {
                Warning("Trailing ',' in object");
            }
        }
        else if (*Parser->It != '}')
        {
            Error("Expected either ',' or '}' after object property but got '%c'", *Parser->It);
        }
        ++Count;
    }
    ++Parser->It;
    if (Parser->Client.OnObjectEnd)
    {
        Parser->Client.OnObjectEnd(Parser->Client.Data, Count);
    }
}

void
ParseTokenNoEatSpace(parser *Parser)
{
top:
    switch (*Parser->It)
    {
        case '{':
        {
            ParseObject(Parser);
        } break;

        case '[':
        {
            ParseArray(Parser);
        } break;

        case '"':
        {
            ParseString(Parser);
        } break;

        case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
        case 'g': case 'h': case 'i': case 'j': case 'k': case 'l':
        case 'm': case 'n': case 'o': case 'p': case 'q': case 'r':
        case 's': case 't': case 'u': case 'v': case 'w': case 'x':
        case 'y': case 'z':

        case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
        case 'G': case 'H': case 'I': case 'J': case 'K': case 'L':
        case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R':
        case 'S': case 'T': case 'U': case 'V': case 'W': case 'X':
        case 'Y': case 'Z':

        case '_':
        {
            ParseLiteral(Parser);
        } break;

        case '0': case '1': case '2': case '3':
        case '4': case '5': case '6': case '7':
        case '8': case '9':

        case '+': case '-':

        case '.':
        {
            ParseNumber(Parser);
        } break;

        case '/':
        {
            IgnoreComment(Parser);
            goto top;
        } break;
    }
}

void
ParseToken(parser *Parser)
{
    EatSpace(Parser);
    ParseTokenNoEatSpace(Parser);
    EatSpace(Parser);
}

void
OnNumber(void *Data, int64_t I, uint64_t U, double D, bool IsNegative, bool IsFloat)
{
    if (IsFloat)
    {
        printf("%f", D);
    }
    else if (IsNegative)
    {
        printf("%ld", I);
    }
    else
    {
        printf("%lu", U);
    }
    if (*(bool *) Data)
    {
        printf(", ");
    }
}

void
OnString(void *Data, const char *Start, size_t Length)
{
    printf("%.*s", (int) Length, Start);
    if (*(bool *) Data)
    {
        printf(", ");
    }
}

void
OnArrayStart(void *Data)
{
    printf("[ ");
    *(bool *) Data = true;
}

void
OnArrayEnd(void *Data, size_t Count)
{
    printf(" ]%lu ", Count);
}

void
OnObjectStart(void *Data)
{
    printf("{ ");
    *(bool *) Data = true;
}

void
OnObjectEnd(void *Data, size_t Count)
{
    printf(" }%lu ", Count);
}

void
ParseJson(const char *Json)
{
    bool Data = false;
    parser Parser = {};
    Parser.It = Json;
    Parser.Client.OnNumber = OnNumber;
    Parser.Client.OnString = OnString;
    Parser.Client.OnArrayStart = OnArrayStart;
    Parser.Client.OnArrayEnd = OnArrayEnd;
    Parser.Client.OnObjectStart = OnObjectStart;
    Parser.Client.OnObjectEnd = OnObjectEnd;
    Parser.Client.Data = (void *) &Data;
    ParseToken(&Parser);
    printf("\n");
}

void
ParseNumberTest()
{
    const char *Json = "980";
    ParseJson(Json);
    Json = "+1000000";
    ParseJson(Json);
    Json = "1234567890";
    ParseJson(Json);

    Json = "-980";
    ParseJson(Json);
    Json = "-1000000";
    ParseJson(Json);
    Json = "-1234567890";
    ParseJson(Json);

    Json = ".980";
    ParseJson(Json);
    Json = "+.980";
    ParseJson(Json);
    Json = "-.980";
    ParseJson(Json);
    Json = "123.456";
    ParseJson(Json);
    Json = "+123.456";
    ParseJson(Json);
    Json = "-123.456";
    ParseJson(Json);
}

void
ParseStringTest()
{
    const char *Json = "\"hello, world\"";
    ParseJson(Json);
}

void
ParseLiteralTest()
{
    const char *Json = "null";
    ParseJson(Json);
    Json = "true";
    ParseJson(Json);
    Json = "false";
    ParseJson(Json);
    Json = "__NAME_someasdf";
    ParseJson(Json);
}

void
ParseArrayTest()
{
    // const char *Json = "[ 123, \"hello\", 90.2 ]";
    // value Root = ParseJson(Json);
    // assert(Root.Type == VALUE_ARRAY);
    // assert(Root.Array.Value);
    // assert(Root.Array.Value->Type == VALUE_NUMBER);
    // assert(Root.Array.Value->Number.Type == NUMBER_UNSIGNED);
    // assert(Root.Array.Value->Number.U == 123ULL);
    
    // assert(Root.Array.Next);
    // assert(Root.Array.Next->Value);
    // assert(Root.Array.Next->Value->Type == VALUE_LITERAL);
    // assert(Root.Array.Next->Value->Literal.Ptr);
    // assert(Root.Array.Next->Value->Literal.EqualTo("hello"));

    // assert(Root.Array.Next->Next);
    // assert(Root.Array.Next->Next->Value);
    // assert(Root.Array.Next->Next->Value->Type == VALUE_NUMBER);
    // assert(Root.Array.Next->Next->Value->Number.Type == NUMBER_DOUBLE);
    // assert(Root.Array.Next->Next->Value->Number.D == 90.2);
    const char *Json = "[]";
    ParseJson(Json);
    Json = "   [   ]   ";
    ParseJson(Json);
    Json = "[  123, -123,   123.123,    \"hello\", true, false, null ]";
    ParseJson(Json);
    Json = "[ 123, -123, 123.123, [ true, false, null ] ]";
    ParseJson(Json);
}

void
ParseObjectTest()
{
    // const char *Json = "{ \"width\": 1440, \"height\": 900.11, \"name\": \"Main Window\" }";
    // value Root = ParseJson(Json);
    // assert(Root.Type == VALUE_OBJECT);
    // assert(Root.Object.Name.Ptr);
    // assert(Root.Object.Name.EqualTo("width"));
    // assert(Root.Object.Value);
    // assert(Root.Object.Value->Type == VALUE_NUMBER);
    // assert(Root.Object.Value->Number.Type == NUMBER_UNSIGNED);
    // assert(Root.Object.Value->Number.U == 1440ULL);

    // assert(Root.Object.Next);
    // assert(Root.Object.Next->Name.Ptr);
    // assert(Root.Object.Next->Name.EqualTo("height"));
    // assert(Root.Object.Next->Value);
    // assert(Root.Object.Next->Value->Type == VALUE_NUMBER);
    // assert(Root.Object.Next->Value->Number.Type == NUMBER_DOUBLE);
    // assert(Root.Object.Next->Value->Number.D == 900.11);

    // assert(Root.Object.Next->Next);
    // assert(Root.Object.Next->Next->Name.Ptr);
    // assert(Root.Object.Next->Next->Name.EqualTo("name"));
    // assert(Root.Object.Next->Next->Value);
    // assert(Root.Object.Next->Next->Value->Type == VALUE_LITERAL);
    // assert(Root.Object.Next->Next->Value->Literal.Ptr);
    // assert(Root.Object.Next->Next->Value->Literal.EqualTo("Main Window"));
    const char *Json = "{}";
    ParseJson(Json);
    Json = "   {   }  ";
    ParseJson(Json);
    Json = "{ \"prop1\": \"value\", \"prop2\": 123.2 }";
    ParseJson(Json);
    Json = "{ \"prop\": [ true, false ] }";
    ParseJson(Json);
}

void
ParseJsonTest()
{
    const char *Json = R"(
// first comment
{
    // second comment
    "auto_complete": false, // third comment
    "close_windows_when_empty": false,
    "color_scheme": "Packages/Colorsublime - Themes/CarbonightBlue.tmTheme",
    "draw_indent_guides": false,
    "draw_white_space": "none",
    "find_selected_text": true,
    "fold_buttons": false,
    "font_face": "Liberation Mono",
    "font_options": // fourth comment
    [ // fifth comment
        "no_italic", // sixth comment
        "no_round"
        // seventh comment
    ],
    "font_size": 18,
    "gpu_window_buffer": true,
    "gutter": false,
    "highlight_line": true,
    "ignored_packages":
    [
        "Vintage"
    ],
    "indent_subsequent_lines": false,
    "indent_to_bracket": true,
    "index_files": true,
    "line_numbers": false,
    "line_padding_bottom": 0,
    "line_padding_top": 0,
    "margin": 0,
    "mini_diff": false,
    "move_to_limit_on_up_down": false,
    "open_file_in_new_window": false,
    "remember_full_screen": true,
    "scroll_past_end": true,
    "shift_tab_unindent": true,
    "theme": "Adaptive.sublime-theme",
    "translate_tabs_to_spaces": true
})";
    ParseJson(Json);
}

int
main() {
    ParseNumberTest();
    ParseStringTest();
    ParseLiteralTest();
    ParseArrayTest();
    ParseObjectTest();
    ParseJsonTest();
}
