#pragma once

#include <stdlib.h>
#include <stdarg.h>

bool
IsSpace(char C)
{
    return C == ' ' || C == '\t' || C == '\n' || C == '\r';
}

bool
IsNumber(char C)
{
    return C >= '0' && C <= '9';
}

bool
IsAlpha(char C)
{
    return (C >= 'a' && C <= 'z') || (C >= 'A' && C <= 'Z');
}

bool
IsAlphaNumeric(char C)
{
    return IsAlpha(C) || IsNumber(C);
}

void
Warning(const char *Format, ...)
{
    va_list Args;
    va_start(Args, Format);
    vprintf(Format, Args);
    va_end(Args);
}

void
Error(const char *Format, ...)
{
    va_list Args;
    va_start(Args, Format);
    vprintf(Format, Args);
    va_end(Args);
    exit(1);
}
